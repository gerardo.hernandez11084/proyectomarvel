import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Heroe } from "../interfaces/heroe.interface";
import 'rxjs/Rx';
//import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  heroesURL: string = "https://heroesapp-3c514.firebaseio.com/heroes.json";
  heroeURL: string = "https://heroesapp-3c514.firebaseio.com/heroes/";
  
  constructor( private http:Http) { }

  nuevoHeroe (heroe:Heroe){
    let body = JSON.stringify(heroe);
    let headers = new Headers({
      'Contect-Type':'application/json'
    });
    
    return this.http.post(this.heroesURL, body, { headers:headers}).pipe(map( res => {
      console.log(res.json());
      return res.json();
    }));
  }

  actualizarHeroe (heroe:Heroe, key$: string){
    let body = JSON.stringify(heroe);
    let headers = new Headers({
      'Contect-Type':'application/json'
    });

    let url = `${ this.heroeURL }/${key$}.json`;
    
    return this.http.put(url, body, { headers:headers}).pipe(map( res => {
      console.log(res.json());
      return res.json();
    }));
  }

  getHeroe (key$:string){
    let url = `${ this.heroeURL }/${key$}.json`;
    return this.http.get(url).map( res => res.json() )
  }

  getHeroes (){
    return this.http.get(this.heroesURL).map( res => res.json() )
  }

  borrarHeroe(key$:string){
    let url = `${ this.heroeURL }/${key$}.json`;
    return this.http.delete(url).map(resultado => resultado.json() )
  }

}
